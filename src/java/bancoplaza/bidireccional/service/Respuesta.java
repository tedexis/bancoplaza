/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoplaza.bidireccional.service;

/**
 *
 * @author jalfonzo
 */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuesta", propOrder = {"codigoRespuesta", "respuestaCliente", "respuestaSoporte"})
public class Respuesta {
  protected String codigoRespuesta;
  
  protected String respuestaCliente;
  
  protected String respuestaSoporte;
  
  public String getCodigoRespuesta() {
    return this.codigoRespuesta;
  }
  
  public void setCodigoRespuesta(String value) {
    this.codigoRespuesta = value;
  }
  
  public String getRespuestaCliente() {
    return this.respuestaCliente;
  }
  
  public void setRespuestaCliente(String value) {
    this.respuestaCliente = value;
  }
  
  public String getRespuestaSoporte() {
    return this.respuestaSoporte;
  }
  
  public void setRespuestaSoporte(String value) {
    this.respuestaSoporte = value;
  }
}
