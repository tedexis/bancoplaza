/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoplaza.bidireccional.service;

/**
 *
 * @author jalfonzo
 */
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "WSSmsBidireccionalService", targetNamespace = "http://servicio.smsbidireccional.bancoplaza.com/", wsdlLocation = "http://10.100.3.50:9080/smsbidireccionaltd/WSSmsBidireccionalService?wsdl")
public class WSSmsBidireccionalService extends Service {
  private static final URL WSSMSBIDIRECCIONALSERVICE_WSDL_LOCATION;
  
  private static final WebServiceException WSSMSBIDIRECCIONALSERVICE_EXCEPTION;
  
  private static final QName WSSMSBIDIRECCIONALSERVICE_QNAME = new QName("http://servicio.smsbidireccional.bancoplaza.com/", "WSSmsBidireccionalService");
  
  static {
    URL url = null;
    WebServiceException e = null;
    try {
      url = new URL("http://10.100.3.50:9080/smsbidireccionaltd/WSSmsBidireccionalService?wsdl");
    } catch (MalformedURLException ex) {
      e = new WebServiceException(ex);
    } 
    WSSMSBIDIRECCIONALSERVICE_WSDL_LOCATION = url;
    WSSMSBIDIRECCIONALSERVICE_EXCEPTION = e;
  }
  
  public WSSmsBidireccionalService() {
    super(__getWsdlLocation(), WSSMSBIDIRECCIONALSERVICE_QNAME);
  }
  
  public WSSmsBidireccionalService(WebServiceFeature... features) {
    super(__getWsdlLocation(), WSSMSBIDIRECCIONALSERVICE_QNAME, features);
  }
  
  public WSSmsBidireccionalService(URL wsdlLocation) {
    super(wsdlLocation, WSSMSBIDIRECCIONALSERVICE_QNAME);
  }
  
  public WSSmsBidireccionalService(URL wsdlLocation, WebServiceFeature... features) {
    super(wsdlLocation, WSSMSBIDIRECCIONALSERVICE_QNAME, features);
  }
  
  public WSSmsBidireccionalService(URL wsdlLocation, QName serviceName) {
    super(wsdlLocation, serviceName);
  }
  
  public WSSmsBidireccionalService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
    super(wsdlLocation, serviceName, features);
  }
  
  @WebEndpoint(name = "WSSmsBidireccionalPort")
  public WSSmsBidireccional getWSSmsBidireccionalPort() {
    return getPort(new QName("http://servicio.smsbidireccional.bancoplaza.com/", "WSSmsBidireccionalPort"), WSSmsBidireccional.class);
  }
  
  @WebEndpoint(name = "WSSmsBidireccionalPort")
  public WSSmsBidireccional getWSSmsBidireccionalPort(WebServiceFeature... features) {
    return getPort(new QName("http://servicio.smsbidireccional.bancoplaza.com/", "WSSmsBidireccionalPort"), WSSmsBidireccional.class, features);
  }
  
  private static URL __getWsdlLocation() {
    if (WSSMSBIDIRECCIONALSERVICE_EXCEPTION != null)
      throw WSSMSBIDIRECCIONALSERVICE_EXCEPTION; 
    return WSSMSBIDIRECCIONALSERVICE_WSDL_LOCATION;
  }
}