/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoplaza.bidireccional.service;

/**
 *
 * @author jalfonzo
 */
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;

@WebService(name = "WSSmsBidireccional", targetNamespace = "http://servicio.smsbidireccional.bancoplaza.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({ObjectFactory.class})
public interface WSSmsBidireccional {
  @WebMethod
  @WebResult(partName = "return")
  @Action(input = "http://servicio.smsbidireccional.bancoplaza.com/WSSmsBidireccional/recibeSMSRequest", output = "http://servicio.smsbidireccional.bancoplaza.com/WSSmsBidireccional/recibeSMSResponse")
  Respuesta recibeSMS(@WebParam(name = "clave", partName = "clave") String paramString1, @WebParam(name = "codPais", partName = "codPais") int paramInt1, @WebParam(name = "codArea", partName = "codArea") int paramInt2, @WebParam(name = "numRem", partName = "numRem") int paramInt3, @WebParam(name = "mensaje", partName = "mensaje") String paramString2, @WebParam(name = "idMensaje", partName = "idMensaje") String paramString3);
}
