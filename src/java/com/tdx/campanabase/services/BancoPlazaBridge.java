/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.services;

import com.tdx.campaigntools.objects.SMSRequest;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import bancoplaza.bidireccional.service.*;

/**
 *
 * @author jalfonzo
 */
public class BancoPlazaBridge {

    static boolean debugMode = false;

    public static void printDebugMessage(String message) {
        if (debugMode) {
            printMessage(message);
        }
    }

    public static void printMessage(String message) {
        System.out.println("Thread:" + Thread.currentThread().getId() + " P2P BANCO PLAZA: " + message);
    }

    public static boolean validarMensaje(String msg, String regex) {
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(msg);
        return matcher.find();
    }

    private static SOAPMessage createSOAPRequest(String soapMessageURI, String metodoSoap, String usuario, String clave, String codCanal, String nroEmisor, String nroBeneficiario, String identificacionBeneficiario, String monto, String conceptoPago, String abaBancoReceptor, String palabraClaveSMS) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("medconecta", soapMessageURI);
        printDebugMessage("" + metodoSoap);
        printDebugMessage("usuario: " + usuario);
        printDebugMessage("clave: " + clave);
        printDebugMessage("codCanal: " + codCanal);
        printDebugMessage("nroEmisor: " + nroEmisor);
        printDebugMessage("nroBeneficiario: " + nroBeneficiario);
        printDebugMessage("identificacionBeneficiario : " + identificacionBeneficiario);
        printDebugMessage("monto: " + monto);
        printDebugMessage("conceptoPago: " + conceptoPago);
        printDebugMessage("abaBancoReceptor: " + abaBancoReceptor);
        printDebugMessage("palabraClaveSMS:" + palabraClaveSMS);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(metodoSoap, "medconecta");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("usuario", "medconecta");
        soapBodyElem1.addTextNode(usuario);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("clave", "medconecta");
        soapBodyElem2.addTextNode(clave);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("codCanal", "medconecta");
        soapBodyElem3.addTextNode(codCanal);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("nroEmisor", "medconecta");
        soapBodyElem4.addTextNode(nroEmisor);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("nroBeneficiario", "medconecta");
        soapBodyElem5.addTextNode(nroBeneficiario);
        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("identificacionBeneficiario", "medconecta");
        soapBodyElem6.addTextNode(identificacionBeneficiario);
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("monto", "medconecta");
        soapBodyElem7.addTextNode(monto);
        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("conceptoPago", "medconecta");
        soapBodyElem8.addTextNode(conceptoPago);
        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("abaBancoReceptor", "medconecta");
        soapBodyElem9.addTextNode(abaBancoReceptor);
        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("palabraClaveSMS", "medconecta");
        soapBodyElem10.addTextNode(palabraClaveSMS);
        soapMessage.saveChanges();
        return soapMessage;
    }

    public static String process(HashMap<String, String> mp, HashMap<String, String> mr, SMSRequest request) throws Exception {
        String result = "";
        try {
            String debugProperty = mp.get("debug");
            if (debugProperty != null && debugProperty.equalsIgnoreCase("true")) {
                debugMode = true;
            }
            String msg_test = request.getTexto();
            msg_test = msg_test.replaceAll(" +", " ");
            String number = request.getNumero();
            if (validarMensaje(msg_test, mp.get("regex_validation"))) {
                String url = ((String) mp.get("soapEndpoind")).trim();
                printMessage("llamando al servicio: " + url);
                WSSmsBidireccionalService WSSms = new WSSmsBidireccionalService(new URL(url));
                String numberphone = request.getNumero();
                String clave = ((String) mp.get("clave")).trim();
                int codePais = Integer.parseInt(numberphone.substring(0, 2));
                int codeArea = Integer.parseInt(numberphone.substring(2, 5));
                int numRem = Integer.parseInt(numberphone.substring(5));
                String mensaje = request.getTexto();
                String idMensaje = request.getIdMensaje();
                try {
                    Respuesta recibeSMS = WSSms.getWSSmsBidireccionalPort().recibeSMS(clave, codePais, codeArea, numRem, mensaje, idMensaje);
                    printMessage("Codigo de respuesta del WS: " + recibeSMS.getCodigoRespuesta());
                    result = recibeSMS.getRespuestaCliente();
                    if (result.trim().isEmpty()) {
                        printMessage("Respuesta del WS vacía, se responde mensajes de error configurado");
                        result = mr.get("mensaje_empty");
                    }
                } catch (Exception e) {
                    printMessage(e.getMessage());
                    e.printStackTrace();
                    result = mr.get("mensaje_error");
                }
                
               
            } else {
                printMessage("Mensaje rechazado por REGEX " + (String) mr.get("mensaje_rejected"));
                result = mr.get("mensaje_rejected");
            }
        } catch (Exception ex) {
            result = mr.get("mensaje_error");
            printMessage("Exception: ");
            ex.printStackTrace();
        }
        return result;
    }
}
